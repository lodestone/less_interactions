require 'test_helper'
# OBSERVATION: Needs better test examples. All this .object shit sucks.
#              Let's find an example use case with super heroes or something.
class InteractionTest < Test::Unit::TestCase

  include Less

  module CrimeFighter

    include Less

    class KillParents < Interaction
      def run
      end
    end

    class TrainInMartialArts < Interaction
      def run
      end
    end

    class EngageBadGuys < Interaction
      expects :badguy

      def run
      end
    end

    class TakeOnSidekick < Interaction
      expects :sidekick, :allow_nil => true
      def run
      end
    end

    class TeamUp < Interaction
      def run; end
    end

  end

  should "not be able to run an interaction without run defined" do
    class InteractionWithoutRun < Interaction; end
    assert_raise(InvalidInteractionError) { InteractionWithoutRun.run }
  end

  should "be able to run an interaction with run defined" do
    assert_nothing_raised { CrimeFighter::KillParents.run }
  end

  should "run an interaction with an options hash" do
    assert_nothing_raised { CrimeFighter::TrainInMartialArts.run(:martial_arts_style => "ninjistu") }
  end

  should "call the run instance method when running an interaction" do
    CrimeFighter::TrainInMartialArts.any_instance.expects(:run)
    CrimeFighter::TrainInMartialArts.run
  end

  should "fail if an expected parameter is not found" do
    assert_raise(MissingParameterError) { CrimeFighter::EngageBadGuys.run }
  end

  should "fail if an expected parameter is nil" do
    assert_raise(MissingParameterError) { CrimeFighter::EngageBadGuys.run(:badguy => nil) }
  end

  should "run if an expected parameter is found" do
    assert_nothing_raised { CrimeFighter::EngageBadGuys.run(:badguy => "Super Villian") }
  end

  should "run if an expected parameter is found, even if it is false" do
    assert_nothing_raised { CrimeFighter::EngageBadGuys.run(:badguy => false) }
  end

  should "run if an expected parameter is found, even if it is nil, if the option is specified" do
    assert_nothing_raised { CrimeFighter::TakeOnSidekick.run(:sidekick => nil) }
  end
  
  should "set ivars from options on initialize" do
    teamup = CrimeFighter::TeamUp.new(:who => "Super Guy", :where => "Metrotham City")
    assert_equal "Super Guy",      teamup.instance_variable_get(:@who)
    assert_equal "Metrotham City", teamup.instance_variable_get(:@where)
  end
  
  
  should "Convert first param to context on initialize" do
    teamup = CrimeFighter::TeamUp.new("Super Guy", :where => "Metrotham City")
    assert_equal "Super Guy", teamup.instance_variable_get(:@context)
    assert_equal "Metrotham City", teamup.instance_variable_get(:@where)
  end
  
  should "be able to fake out expects" do
    class FakeoutExpects < Less::Interaction
      expects :object
      def initialize params, options
        super :object => params[:object_id].to_s #or a finder or something instead of to_s
      end
      def run; self; end #return self just so I can test the value
    end
    assert_nothing_raised do
      x = FakeoutExpects.run(:object_id => 1)
      assert_equal "1", x.instance_variable_get(:@object)
    end
  end
  
  should "be able to override an expects" do
    
    class OverrideExpects < Less::Interaction
      expects :object, allow_nil: true
      
      def run; self; end #return self just so I can test the value
      def object; "YES!";  end
    end
    
    x = OverrideExpects.new(1, object: "no :(").run
    assert_equal "YES!", x.object
  end
  
  
  should "get a method that returns nil from an allow_nil expects" do
    
    class NilExpects < Less::Interaction
      expects :object, allow_nil: true
      def run; self; end #return self just so I can test the value
    end
    
    x = NilExpects.run
    assert_equal nil, x.object
  end
  
end
