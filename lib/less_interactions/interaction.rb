
module Less
  class Interaction
    # Initialize the objects for an interaction. 
    # @param [Object] context The context for running an interaction. Optional param.
    # @param [Hash] options   The options are passed when running an interaction. Optional param.
    def initialize(context = {}, options = {})
      #the param context = {} is to allow for interactions with no context
      if context.is_a? Hash
        options.merge! context #context is not a Context so merge it in
      else
        options[:context] = context # add context to the options so will get the ivar and getter
      end
      instantiate_options(options)
    end
    
    def expectations_set
      self.class.expectations
    end
    private :expectations_set

    def nil_expectations
      allow_nils = expectations_set.select {|name, expectation_options| expectation_options[:allow_nil] }
      allow_nils.inject({}) do |hash, _nil|
        hash[_nil.first] = nil
        hash
      end
    end
    private :nil_expectations

    def instantiate_options(options)
      options.merge(nil_expectations).each do |name, value|
        instance_variable_set "@#{name}", value
        eval "def #{name}; instance_variable_get :@#{name}; end"
      end
    end
    private :instantiate_options

    # Definition of the interaction itself. You should override this in your interactions
    #
    # The default implementation raises an {InvalidInteractionError}
    def run
      raise InvalidInteractionError, "You most override the run instance method in #{self.class}"
    end

    # Run your interaction.
    # @param [Object] context   
    # @param [Hash] options   
    #
    # This will initialize your interaction with the options you pass to it and then call its {#run} method.
    def self.run(context = {}, options = {})
      me = new(context, options)
      # QUESTION: Why are we .sending :expectations_met? vs calling it via dot operator?
      raise MissingParameterError unless me.send :expectations_met?
      me.run
    end

    # Expect certain parameters to be present. If any parameter can't be found, a {MissingParameterError} will be raised. 
    # @overload expects(*parameters)
    #   @param *parameters A list of parameters that your interaction expects to find. 
    # @overload expects(*parameters, options)
    #   @param *parameters A list of parameters that your interaction expects to find. 
    #   @param options A list of options for the exclusion
    #   @option options :allow_nil Allow nil values to be passed to the interaction, only check to see whether the key has been set
 
    def self.expects(*parameters)
      if parameters.last.is_a?(Hash)
        options = parameters.pop
      else
        options = {}
      end
      parameters.each { |param| add_expectation(param, options) }
    end
    
    
    
    private

    def self.add_expectation(parameter, options)
      expectations[parameter] = options
    end

    def expectations_met?
      self.class.expectations.each do |param, param_options|
        unless param_options[:allow_nil]
          raise MissingParameterError, "Parameter empty   :#{param.to_s}" if instance_variable_get("@#{param}").nil?
        end
      end
    end

    def self.expectations
      @expectations ||= {}
    end
  end


  class InvalidInteractionError < StandardError; end
  class MissingParameterError < StandardError; end
end
